from django.contrib import admin

from cook.models import ActionClusterType, ActionCluster, IngredientClusterType, IngredientCluster


class ActionClusterTypeAdmin(admin.ModelAdmin):
    fields = ['name', 'description']


class ActionClusterAdmin(admin.ModelAdmin):
    fields = ['name', 'description', 'type', 'ingredients', 'parents']
    list_filter = ['type']


class IngredientClusterTypeAdmin(admin.ModelAdmin):
    fields = ['name', 'description']


class IngredientClusterAdmin(admin.ModelAdmin):
    fields = ['name', 'description', 'type', 'actions', 'parents']
    list_filter = ['type']


admin.site.register(ActionClusterType, ActionClusterTypeAdmin)
admin.site.register(ActionCluster, ActionClusterAdmin)
admin.site.register(IngredientClusterType, IngredientClusterTypeAdmin)
admin.site.register(IngredientCluster, IngredientClusterAdmin)
