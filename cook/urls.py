from django.conf.urls import patterns, url
from django.conf import settings
from django.conf.urls.static import static

from cook import views


urlpatterns = patterns(
    '',
    url(r'^(?P<pk>\d+)/$', views.DetailView.as_view(), name="detail")
)