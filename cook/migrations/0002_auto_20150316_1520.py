# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('cook', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='actioncluster',
            name='type',
            field=models.ForeignKey(default=None, to='cook.ActionClusterType'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='ingredientcluster',
            name='type',
            field=models.ForeignKey(default='1', to='cook.IngredientClusterType'),
            preserve_default=False,
        ),
    ]
