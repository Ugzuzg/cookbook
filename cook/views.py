from django.shortcuts import render
from django.views import generic

from cook.models import ActionCluster, IngredientCluster


class DetailView(generic.DetailView):
    model = IngredientCluster
    template_name = "cook/detail.html"

    def get_context_data(self, **kwargs):
        context = super(DetailView, self).get_context_data(**kwargs)
        flat = []
        queue = [context["ingredientcluster"]]
        while len(queue) > 0:
            i = queue[0]
            for action in i.actions.all():
                flat.append((action, i))
                for ingredient in action.ingredients.all():
                    queue.append(ingredient)
            queue.pop(0)
        context["flat_actions"] = reversed(flat)
        return context