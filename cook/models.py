from django.db import models


class ActionClusterType(models.Model):
    name = models.CharField(max_length=200)
    description = models.CharField(max_length=1000)

    def __str__(self):
        return self.name


class ActionCluster(models.Model):
    type = models.ForeignKey(ActionClusterType)
    name = models.CharField(max_length=200)
    description = models.CharField(max_length=1000)
    ingredients = models.ManyToManyField("IngredientCluster", null=True, blank=True)
    parents = models.ManyToManyField("self", null=True, blank=True)

    def __str__(self):
        return self.name


class IngredientClusterType(models.Model):
    name = models.CharField(max_length=200)
    description = models.CharField(max_length=1000)

    def __str__(self):
        return self.name


class IngredientCluster(models.Model):
    type = models.ForeignKey(IngredientClusterType)
    name = models.CharField(max_length=200)
    description = models.CharField(max_length=1000)
    actions = models.ManyToManyField(ActionCluster, null=True, blank=True)
    parents = models.ManyToManyField("self", null=True, blank=True)

    def has_actions(self):
        return self.actions.count() > 0

    def __str__(self):
        return self.name