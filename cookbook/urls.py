from django.conf.urls import patterns, include, url
from django.contrib import admin

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'cookbook.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),
    url(r'^', include('cook.urls', namespace='cook')),
    url(r'^admin/', include(admin.site.urls)),
)
